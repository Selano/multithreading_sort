#include <iostream>
#include <thread>
#include <future>
#include <functional>
#include <queue>
#include "../include/sort_function.hpp"

int main() {
    int *ptr_array = new int[20];
    int size = 20; // sizeof(ptr_array) / sizeof(int);
    std::cout << "Array size: " << size << std::endl;
    for(int i = 0; i < size; i++) {
        ptr_array[i] = rand() % 100;
    }

    std::cout << "Array: ";
    for(int i = 0; i < size; i++) {
        std::cout << ptr_array[i] << " ";
    }
    std::cout << std::endl;

    std::queue<std::future<void>> fl;
    for(int i = 0; i < (size / 4); i++) {
        fl.push(std::async(std::launch::async, std::bind(quicksort, (ptr_array + i * 4), 4)));
    }

    while (!fl.empty())
    {
        (fl.front()).wait();
        fl.pop();
    }

    std::cout << "Array(first sort): ";
    for(int i = 0; i < size; i++) {
        std::cout << ptr_array[i] << " ";
    }
    std::cout << std::endl;

    quicksort(ptr_array, size);

    std::cout << "Array(last sort): ";
    for(int i = 0; i < size; i++) {
        std::cout << ptr_array[i] << " ";
    }
    std::cout << std::endl;

    delete[] ptr_array;
}